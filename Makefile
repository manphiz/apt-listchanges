include common.mk
all::

SHELL=/bin/bash

# install config
install:: all
	install -d $(DESTDIR)/etc/apt/apt.conf.d
	install -m 644 debian/apt.conf $(DESTDIR)/etc/apt/apt.conf.d/20listchanges

# Make sure we don't have snapshots enabled outside experimental
test::
	(head -1 debian/changelog | grep experimental) || \
	python3 -c 'from apt_listchanges.debconf_helper import snapshots_wanted; assert not snapshots_wanted, "Snapshots should only be enabled in debconf_helper.py for experimental builds"'

# Test for syntax errors
test::
	python3 -mcompileall setup.py tests/*.py apt_listchanges/*.py

clean::
	dh clean --buildsystem=pybuild

dh_clean:: ; -rm -rf *.egg-info debian/.debhelper debian/*.debhelper
dh_clean:: ; -rm -rf debian/apt-listchanges debian/apt-listchanges.substvars
dh_clean:: ; -rm -rf debian/*stamp debian/files .pybuild
dy_clean::
	$(MAKE) -C po clean
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C doc clean
endif


all install update-po::
	$(MAKE) -C po $@
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C doc $@
endif

update-po:: ; debconf-updatepo

lint::
# | cat because otherwise flake8 and pytest spit out terminal escape sequences
	set -o pipefail; flake8 | cat

lint::
	set -o pipefail; python3 -m pylint */*.py | cat

test::
	python3 -m pytest
