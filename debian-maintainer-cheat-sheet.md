<!-- Copyright (C) 2024 THE PACKAGE'S COPYRIGHT HOLDER -->

# Debian package maintainer cheat-sheet for apt-listchanges

Since `apt-listchanges` is the first (and currently only) Debian
package I am maintaining, and since I work on it intermittently, in
the gaps between when I am actively working on it I tend to forget all
the details about how to do stuff. This document is attempt to capture
the key points I need to know to be able to pick up the work when I
need to.

## Current gotchas

AS of 2024-09-01, there's a bug which causes `piuparts` to complain
about `/etc/localtime` being missing (see [bug 1080295][bug1080295].
To work around this, the `tzdata` package needs to be pre-installed in
the chroot with `sudo sbuild-apt unstable apt-get install tzdata`.

## Documentation

There are links to all the Debian developer documentation at
[here][doc-index].

The [Developer's Reference][dev-reference] can be viewed online or
installed via the `developers-reference` package.

The [Guide for Debian Maintainers][guide], which is a good
tutorial of all the essential package maintainence stuff, can be
viewed online or installed via the `debmake-doc` package.

The [New Maintainers' Guide][new-maint] can be viewed online or
installed via the `maint-guide` package.

A tutorial with a different approach, [Introduction to Debian
packaging][intro], can be viewed online or installed via the
`packaging-tutorial` package.

Once you get the hang of the basics and you're starting to deal with
all the weird details, the tooling is going to occasionally complain
to you that you're doing something you're not allowed to do, either
because the rules have changed or you broke something. You may need to
consult the [Policy Manual][policy] to figure out what it's
complaining about (also available in the `debian-policy` package).

## Building

Running `make` with no arguments runs various checks on the
translations and documentation files to make sure they're correct. It
doesn't actually build packages. One thing this does is add new
strings to the translation template files as needed. These can be
distributed to translators as described below to get them translated.

Running `make lint` runs flake8 and pylint on all the source code to
ensure that everything is clean. You shouldn't release if there's any
dirt here.

Running `make test` runs all the unit tests. There aren't enough, but
you certainly shouldn't ship unless they're all passing.

Running `debuild` builds packages in your local machine environment,
i.e., it doesn't use a `chroot` or anything. In addition to building
the packages, it runs various tests such as `lintian` and reports on
the results. Pay attention to the warnings and fix them whenever
possible.

Running `sbuild` does the same thing, but in the chroot that you
previously set up. If `sbuild` reports errors that look like they're
unrelated to `apt-listchanges`, then try updating the chroot as
described below and seeing if that makes the errors go away.

## Maintaining the chroot

Run `sudo sbuild-update -udcar unstable` periodically to update all
the packages in the chroot.

If you notice that `sbuild` is installing new packages in the chroot,
you may want to speed things up for next time by pre-installing those
packages into the chroot with `sudo sbuild-apt unstable apt-get
install [packages]`.

**NOTE:** If you follow the instructions in the previous paragraph,
your chroot will be tailored to building `apt-listchanges`, which will
make it hard to get the dependencies right in your control file
if/when you maintain other packages. In that case, you either need to
maintain different chroots for different packages, or recreate the
chroot and stop pre-installing `apt-listchanges` dependencies in it.

## Maintaining the changelog and NEWS file

Note that the build system uses `debian/changelog` to determine the
version number of the package and what release stream it's targeted
for. That means, obviously, that you can't generate a new release
without updating the changelog.

You don't have to mention every change in the changelog. That's what
the git commit history is for. Only user-visible changes need to go
into the changelog. For example, improvements to the build process of
unit tests do not need to be mentioned.

Especially important user-visible changes that should be displayed to
the user on upgrade should be included in `debian/NEWS` in addition to
`debian/changelog`.

Your changelog entries should mention when they resolve bugs. If you
do this properly, then the referenced bugs get closed automatically in
the [Debian bug tracking system][bugs] when the release containing the
fixes goes out. See the old bug references in the changelog for the
proper format, especially when a single changelog entry resolves
multiple bugs.

### Emacs key bindings for editing changelog and NEWS files

You can type `C-h m` in the buffer while editing the changelog to
see all the key bindings, but these are the most important ones:

* Use `C-c C-v` when you're ready go start changelog entries for a new
  release. This creates the header line for the release, with an
  incremented version number, based on the header line for the
  previous release.
* Use `C-c C-a` to add a new item to the list of changes in the
  current release.
* When you're ready to release, use `C-c C-f` to put the footer line
  with the timestamp on the list of changes in this release. If you
  change your mind and need to add more changes afterward, Use `C-c
  C-e` to "unfinalize" the release.

## Releasing

Steps to release:

1. `make` shows no errors.
2. `make lint` shows no code problems.
3. `make test` shows no test failures.
4. Update changelog and NEWS file as needed.
5. `debuild` completes with no problems.
6. `sbuild` completes with no problems.
7. Make sure all changes are committed and pushed to Salsa.
8. Make sure Salsa build pipeline completes successfully.
9. Create and push a tag for the release.
10. Sign the source changes file that sbuild built with `debsign -S`
    (just run that command in the source directory; the built files
    should be in its parent directory).
11. Upload the signed source changes file with `dput mentors
    ../apt-listchanges_[version]_source.changes`.
12. Give your mentor a heads-up that the package is ready for them to
    approve.

## Translations

There are three sets of translations, one for the man page (in
`doc/po`), one for messages printed by the program (in `po`), and one
for the debconf strings (in `debian/po`).
   
You get translation updates as follows:

1. Do a ``make` and `make update-po` to ensure that all of the
   translation files in po, doc/po, and debian/po are up-to-date with
   all of the translation strings that should be in them. Check the
   changes and make sure they're substantive; if they're just
   timestamps and nothing else, throw them away, otherwise commit
   them.
2. Use `podebconf-report-po` to send out the files requiring new
   translations to the appropriate translators. If one of the emails
   bounces because the email address is no longer valid, or the
   translator does not eventually respond with an updated translation,
   you may need to reach out to the translator list for the language
   in question, or do a generic call for translators. See the
   `podebconf-report-po` man page or the package maintainer
   documentation referenced above for additional information.
3. When you get back po files from translators, merge them in,
   something like this:
    * Save the new po file as `[language]-from-translator.po` in the
      directory the old file is (`po` or `doc/po`).
    * Run `msgmerge [language]-from-translator.po [language].po >
      [language]-new.po && mv [language]-new.po [language].po`.
    * Use `git diff` to review the changes and fix any problems you
      see.
    * Remove the `[language]-from-translator.po` file.
    * Commit the changes.
    * Make sure there's a changelog entry that closes the ticket the
      update was submitted in, if it was submitted via a ticket.

## To do

We should add Python typing to all of the source code. I need to fill
in more details here about how that's done.

Now that the `pytest-subprocess` package is included in Debian, we
should be able to write tests much more easily since we can mock out
calls to external commands, so we should add a bunch more tests.

<!---------------------------------------------------------------------------->

[bug1077324]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1077324
[bug1080295]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1080295
[doc-index]: https://www.debian.org/doc/devel-manuals
[dev-reference]: https://www.debian.org/doc/manuals/developers-reference/index.en.html
[guide]: https://www.debian.org/doc/manuals/debmake-doc/index.en.html
[new-maint]: https://www.debian.org/doc/manuals/maint-guide/index.en.html
[intro]: https://www.debian.org/doc/manuals/packaging-tutorial/packaging-tutorial.en.pdf
[policy]: https://www.debian.org/doc/debian-policy/
[bugs]: https://bugs.debian.org/
