# Spanish po-file for apt-listchanges
# Copyright (C) 2001, 2002 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2001, 2002.
# David Martínez <ender@debian.org>
# Ricardo Mones  <mones@aic.uniovi.es>, 2002
#
# - Updates
# Jonatan Porras <jonatanpc8@gmail.com>, 2023.
# Notas de traducción:
# - changelogs: lista de cambios
#
msgid ""
msgstr ""
"Project-Id-Version: apt-listchanges 4.5\n"
"Report-Msgid-Bugs-To: apt-listchanges@packages.debian.org\n"
"POT-Creation-Date: 2024-09-24 12:56-0400\n"
"PO-Revision-Date: 2023-10-24 19:43:13-0500\n"
"Last-Translator: Jonatan Porras <jonatanpc8@gmail.com>\n"
"Language-Team: Debian Spanish Team <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../apt_listchanges/ALCApt.py:57
msgid "APT pipeline messages:"
msgstr "Mensajes de APT pipeline:"

#: ../apt_listchanges/ALCApt.py:64
msgid "Packages list:"
msgstr "Lista de paquetes:"

#: ../apt_listchanges/ALCApt.py:74
msgid ""
"APT_HOOK_INFO_FD environment variable is not defined\n"
"(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::InfoFD set to 20?)"
msgstr ""
"La variable de entorno APT_HOOK_INFO_FD no está definida\n"
"(es Dpkg::Tools::Options::/usr/bin/apt-listchanges::InfoFD está configurado "
"en 20?)"

#: ../apt_listchanges/ALCApt.py:82
msgid "Invalid (non-numeric) value of APT_HOOK_INFO_FD environment variable"
msgstr ""
"Valor no válido (no numérico) de la variable de entorno APT_HOOK_INFO_FD"

#: ../apt_listchanges/ALCApt.py:85
#, python-format
msgid "Will read apt pipeline messages from file descriptor %d"
msgstr "Leerá mensajes apt pipeline del descriptor de archivo %d"

#: ../apt_listchanges/ALCApt.py:90
msgid ""
"APT_HOOK_INFO_FD environment variable is incorrectly defined\n"
"(Dpkg::Tools::Options::/usr/bin/apt-listchanges::InfoFD should be greater "
"than 2)."
msgstr ""
"La variable de entorno APT_HOOK_INFO_FD está definida incorrectamente (Dpkg::"
"Herramientas::Opciones::/usr/bin/apt-listchanges::InfoFD debe ser mayor que "
"2)."

#: ../apt_listchanges/ALCApt.py:98
#, python-format
msgid "Cannot read from file descriptor %(fd)d: %(errmsg)s"
msgstr "No se puede leer el descriptor de archivo %(fd)d: %(errmsg)s"

#: ../apt_listchanges/ALCApt.py:105
msgid ""
"Wrong or missing VERSION from apt pipeline\n"
"(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)"
msgstr ""
"VERSION errónea o inexistente desde la tubería de apt\n"
"(¿Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version vale 2?)"

#: ../apt_listchanges/ALCConfig.py:164
#, python-format
msgid "Unknown configuration file option: %s"
msgstr "Opción desconocida del archivo de configuración: %s"

#: ../apt_listchanges/ALCConfig.py:179
msgid "Usage: apt-listchanges [options] {--apt | filename.deb ...}\n"
msgstr "Modo de uso: apt-listchanges [opciones] {--apt | archivo.deb ...}\n"

#: ../apt_listchanges/ALCConfig.py:186
#, python-format
msgid "Unknown argument %(arg)s for option %(opt)s.  Allowed are: %(allowed)s."
msgstr ""
"Argumento desconocido %(arg)s para la opción %(opt)s. Los permitidos son: "
"%(allowed)s."

#: ../apt_listchanges/ALCConfig.py:200
#, python-format
msgid "%(deb)s does not have '.deb' extension"
msgstr "%(deb)s no tiene extensión '.deb'"

#: ../apt_listchanges/ALCConfig.py:204
#, python-format
msgid "%(deb)s does not exist or is not a file"
msgstr "%(deb)s no existe o no es un archivo"

#: ../apt_listchanges/ALCConfig.py:208
#, python-format
msgid "%(deb)s is not readable"
msgstr "%(deb)s no es legible"

#: ../apt_listchanges/ALCConfig.py:315
msgid "--since=<version> and --show-all are mutually exclusive"
msgstr "--since=<version> y --show-all se excluyen mutuamente"

#: ../apt_listchanges/ALCConfig.py:325
msgid "--since=<version> expects a path to exactly one .deb archive"
msgstr "--since=<version> espera una ruta a exactamente un archivo .deb"

#: ../apt_listchanges/ALCConfig.py:333
msgid "--latest=<N> and --show-all are mutually exclusive"
msgstr "--latest=<N> y --show-all son mutuamente excluyentes"

#: ../apt_listchanges/ALCLog.py:37 ../apt_listchanges/ALCLog.py:46
#, python-format
msgid "apt-listchanges: %(msg)s"
msgstr "apt-listchanges: %(msg)s"

#: ../apt_listchanges/ALCLog.py:41
#, python-format
msgid "apt-listchanges warning: %(msg)s"
msgstr "apt-listchanges advertencia: %(msg)s"

#: ../apt_listchanges/ALCSeenDb.py:154
#, python-format
msgid "Database %(db)s failed to load: %(errmsg)s"
msgstr "La base de datos %(db)s no pudo cargarse: %(errmsg)s"

#: ../apt_listchanges/AptListChangesGtk.py:51
msgid "apt-listchanges: Reading changelogs"
msgstr "apt-listchanges: Leyendo lista de cambioss"

#: ../apt_listchanges/AptListChangesGtk.py:53
msgid "Reading changelogs. Please wait."
msgstr "Leyendo lista de cambios. Por favor espere."

#: ../apt_listchanges/AptListChangesGtk.py:112
msgid "Continue Installation?"
msgstr "¿Continuar con la instalación?"

#: ../apt_listchanges/AptListChangesGtk.py:113
#, fuzzy
#| msgid ""
#| "Select <i>yes</i> to continue with the installation.\n"
#| "Select <i>no</i> to abort the installation."
msgctxt "The 'yes' and 'no' should be translated."
msgid ""
"Select <i>yes</i> to continue with the installation.\n"
"Select <i>no</i> to abort the installation."
msgstr ""
"Seleccione <i>si</i> para continuar con la instalación.\n"
"Seleccione <i>no</i> para cancelar la instalación."

#: ../apt_listchanges/DebianFiles.py:121 ../apt_listchanges/DebianFiles.py:131
#, python-format
msgid "Error processing '%(what)s': %(errmsg)s"
msgstr "Error al procesar '%(what)s': %(errmsg)s"

#: ../apt_listchanges/DebianFiles.py:465
#, python-format
msgid "Ignoring `%s' (seems to be a directory!)"
msgstr "Se ignorará «%s» (¡parece tratarse de un directorio!)"

#: ../apt_listchanges/DebianFiles.py:521
#, python-format
msgid "Extracting changes from %(debfile)s"
msgstr "Extrayendo cambios de %(debfile)s"

#: ../apt_listchanges/DebianFiles.py:565
#, python-format
msgid "Extracting installed changes for %(package)s"
msgstr "Extrayendo cambios instalados para %(package)s"

#: ../apt_listchanges/DebianFiles.py:607
#, python-format
msgid "Calling %(cmd)s to retrieve changelog"
msgstr "Llamando a %(cmd)s para recuperar el registro de cambios"

#: ../apt_listchanges/DebianFiles.py:615
#, python-format
msgid ""
"Unable to retrieve changelog for package %(pkg)s; 'apt-get changelog' failed "
"with: %(errmsg)s"
msgstr ""
"No se puede recuperar el registro de cambios para el paquete %(pkg)s; falló "
"'apt-get changelog' con: %(errmsg)s"

#: ../apt_listchanges/DebianFiles.py:624
#, python-format
msgid ""
"Unable to retrieve changelog for package %(pkg)s; could not run 'apt-get "
"changelog': %(errmsg)s"
msgstr ""
"No se puede recuperar el registro de cambios del paquete %(pkg)s; no se pudo "
"ejecutar 'apt-get changelog': %(errmsg)s"

#: ../apt_listchanges/apt_listchanges.py:168
msgid "Enabled debug output"
msgstr "Salida de depuración habilitada"

#: ../apt_listchanges/apt_listchanges.py:198
#, python-format
msgid "Unknown frontend: %s"
msgstr "Interfaz desconocido: %s"

#: ../apt_listchanges/apt_listchanges.py:214
#, python-format
msgid "Cannot reopen /dev/tty for stdin: %s"
msgstr "No se puede reabrir /dev/tty para stdin: %s"

#: ../apt_listchanges/apt_listchanges.py:235
msgid "apt-listchanges: News"
msgstr "apt-listchanges: Noticias"

#: ../apt_listchanges/apt_listchanges.py:237
msgid "apt-listchanges: Changelogs"
msgstr "apt-listchanges: lista de cambios"

#: ../apt_listchanges/apt_listchanges.py:246
#, python-format
msgid "apt-listchanges: news for %s"
msgstr "apt-listchanges: noticias de %s"

#: ../apt_listchanges/apt_listchanges.py:249
#, python-format
msgid "apt-listchanges: changelogs for %s"
msgstr "apt-listchanges: lista de cambios para %s"

#: ../apt_listchanges/apt_listchanges.py:275
#, python-format
msgid "Received signal %d, exiting"
msgstr "Señal recibida %d, saliendo"

#: ../apt_listchanges/apt_listchanges.py:335
#, python-format
msgid "News for %s"
msgstr "Noticias de %s"

#: ../apt_listchanges/apt_listchanges.py:337
#, python-format
msgid "Changes for %s"
msgstr "Cambios de %s"

#: ../apt_listchanges/apt_listchanges.py:347
msgid "Informational notes"
msgstr "Notas informativas"

#: ../apt_listchanges/apt_listchanges.py:366
#, python-format
msgid "%s: will be newly installed"
msgstr "%s: se instalará por primera vez"

#: ../apt_listchanges/apt_listchanges.py:415
msgid "Binary NMU of"
msgstr "Binario NMU de"

#: ../apt_listchanges/frontends.py:56
msgid "Aborting"
msgstr "Abortando"

#: ../apt_listchanges/frontends.py:61
#, python-format
msgid "Confirmation failed: %s"
msgstr "Error de confirmación: %s"

#: ../apt_listchanges/frontends.py:66
#, python-format
msgid "Mailing %(address)s: %(subject)s"
msgstr "Enviando por correo %(address)s: %(subject)s"

#: ../apt_listchanges/frontends.py:87
#, python-format
msgid "Failed to send mail to %(address)s: %(errmsg)s"
msgstr "Error al enviar correo a %(dirección)s: %(errmsg)s"

#: ../apt_listchanges/frontends.py:97
#, python-format
msgid "The mail frontend needs an installed 'sendmail', using %s"
msgstr "La interfaz de correo necesita un 'sendmail' instalado, usando %"

#: ../apt_listchanges/frontends.py:104
#, python-format
msgid "The mail frontend needs an e-mail address to be configured, using %s"
msgstr ""
"La interfaz de correo necesita configurar una dirección de correo "
"electrónico, utilizando %s"

#: ../apt_listchanges/frontends.py:118
msgid "Available apt-listchanges frontends:"
msgstr "Interfaces de apt-listchanges disponibles:"

#: ../apt_listchanges/frontends.py:121
msgid "Choose a frontend by entering its number: "
msgstr "Elija una interfaz ingresando su número:"

#: ../apt_listchanges/frontends.py:130 ../apt_listchanges/frontends.py:524
#, python-format
msgid "Error: %s"
msgstr "Error: %s"

#: ../apt_listchanges/frontends.py:132
#, python-format
msgid "Using default frontend: %s"
msgstr "Usando la interfaz predeterminada: %s"

#: ../apt_listchanges/frontends.py:190
#, python-format
msgid "$DISPLAY is not set, falling back to %(frontend)s"
msgstr "$DISPLAY no está configurado, volviendo a %(frontend)s"

#: ../apt_listchanges/frontends.py:212
#, python-format
msgid ""
"The gtk frontend needs a working python3-gi,\n"
"but it cannot be loaded. Falling back to %(frontend)s.\n"
"The error is: %(errmsg)s"
msgstr ""
"La interfaz gtk necesita python3-gi que funcione,\n"
"pero no se puede cargar. Volviendo a %(frontend)s.\n"
"El error es: %(errmsg)s"

#: ../apt_listchanges/frontends.py:325
msgid "Do you want to continue? [Y/n] "
msgstr "¿Quiere continuar? [Y/n] "

#: ../apt_listchanges/frontends.py:341 ../apt_listchanges/frontends.py:484
#: ../apt_listchanges/frontends.py:493
msgid "Reading changelogs"
msgstr "Leyendo lista de cambios"

#: ../apt_listchanges/frontends.py:412 ../apt_listchanges/frontends.py:468
#, python-format
msgid "Command %(cmd)s exited with status %(status)d"
msgstr "Command %(cmd)s ha salido con el estado %(status)d"

#: ../apt_listchanges/frontends.py:494
msgid "Done"
msgstr "Hecho."

#: ../apt_listchanges/frontends.py:505
#, python-format
msgid "Found user: %(user)s, temporary directory: %(dir)s"
msgstr "Usuario encontrado: %(user)s, directorio temporal: %(dir)s"

#: ../apt_listchanges/frontends.py:554
#, python-format
msgid "Error getting user from variable '%(envvar)s': %(errmsg)s"
msgstr "Error al obtener el usuario de la variable '%(envvar)s': %(errmsg)s"

#: ../apt_listchanges/frontends.py:561
msgid "Cannot find suitable user to drop root privileges"
msgstr ""
"No se puede encontrar un usuario adecuado para eliminar los privilegios de "
"root"

#: ../apt_listchanges/frontends.py:589
#, python-format
msgid ""
"None of the following directories is accessible by user %(user)s: %(dirs)s"
msgstr ""
"Ninguno de los siguientes directorios es accesible por el usuario "
"%(usuario)s: %(dirs)s"

#: ../apt_listchanges/frontends.py:624
#, fuzzy
#| msgid "press q to quit"
msgctxt "Do not translate the 'q'."
msgid "press q to quit"
msgstr "presione q para salir"

#: ../apt_listchanges/apt-listchanges.ui:13
msgid "List the changes"
msgstr "Lista de Cambios"

#: ../apt_listchanges/apt-listchanges.ui:43
msgid ""
"The following changes are found in the packages you are about to install:"
msgstr ""
"Los siguientes cambios se encuentran en los paquetes que está a punto de "
"instalar:"

#~ msgid "Didn't find any valid .deb archives"
#~ msgstr "No se ha encontrado ningún fichero .deb válido"

#, python-format
#~ msgid "%(pkg)s: Version %(version)s has already been seen"
#~ msgstr "%(pkg)s: La versión %(version)s ya se ha visualizado"

#~ msgid ""
#~ "%(pkg)s: Version %(version)s is lower than version of related packages "
#~ "(%(maxversion)s)"
#~ msgstr ""
#~ "%(pkg)s: La versión %(version)s es inferior a la versión (%(maxversion)s) "
#~ "de los paquetes relacionados (%(maxversion)s)"

#~ msgid "The %s frontend is deprecated, using pager"
#~ msgstr "La interfaz %s está obsoleta, usando el paginador"

#~ msgid "Confirmation failed, don't save seen state"
#~ msgstr "Fallo de confirmación, no se guardará el estado visualizado"

#~ msgid "%s exited with signal %s"
#~ msgstr "%s ha salido con la señal %s"

#~ msgid "Must specify either --apt or filenames to process!"
#~ msgstr "¡Tiene que especificar --apt o bien los paquetes a procesar!"

#~ msgid "Unable to determine package name for file %s"
#~ msgstr "No se puede determinar el nombre del paquete para el fichero %s"

#~ msgid "Unable to determine version for file %s"
#~ msgstr "No se puede determinar la versión para el fichero %s"

#~ msgid "%s: Version %s is already installed"
#~ msgstr "%s: La versión %s ya está instalada"

#~ msgid "%s: Version %s is older than installed version (%s)"
#~ msgstr "%s: La versión %s es más antigua que la versión instalada (%s)"

#~ msgid "Error output follows"
#~ msgstr "La salida de error es la siguiente"

#~ msgid "In order to use the newt frontend, you must install libnewt-perl"
#~ msgstr "Para usar el frontend de newt, necesita instalar libnewt-perl"

#~ msgid "Scanning packages..."
#~ msgstr "Escaneando paquete..."

#~ msgid "Displaying changelogs"
#~ msgstr "Mostrando lista de cambios"

#~ msgid "Unable to open tty for confirmation, assuming yes"
#~ msgstr "No se puede abrir la tty para pedir confirmación, asumiendo sí"

#~ msgid "Unable to open temporary file %s: %s"
#~ msgstr "No se puede abrir archivo temporal %s: %s"

#~ msgid "Changelogs mailed to %s"
#~ msgstr "Lista de cambios enviados a %s"

#~ msgid "Wrong or missing VERSION from apt pipeline"
#~ msgstr "VERSION errónea o inexistente proviniente de la tubería de apt"
