import os

from utils import (
    install_yaml_package,
    make_config,
    package_from_yaml_file,
    setup_filterer,
)

from apt_listchanges.ALCSeenDb import SeenDb
from apt_listchanges.apt_listchanges import process_pkgs
from apt_listchanges.frontends import Frontend


def test_identical_news_changelog(fs):
    fs.add_real_directory(os.path.dirname(__file__))
    install_yaml_package(fs, 'identical-news-changelog-1.yaml')
    seen_db = SeenDb()
    filterer = setup_filterer(seen_db=seen_db)
    config = make_config()
    frontend = Frontend(config, 1)
    pkg = package_from_yaml_file('identical-news-changelog-3.yaml', filterer)
    (news, changelog) = process_pkgs(config, frontend, seen_db, [pkg])
    print("NEWS", news)
    print("CHANGELOG", changelog)
    # 1.3 entry should appear in changelog or news but not both
    in_news = 'identical-news-changelog (1.3-1)' in news
    in_changelog = 'identical-news-changelog (1.3-1)' in changelog
    assert in_news != in_changelog
    assert 'identical-news-changelog (1.2-1)' in changelog
