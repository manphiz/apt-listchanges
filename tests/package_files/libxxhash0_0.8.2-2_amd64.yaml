pkgdata:
  name: libxxhash0
  version: 0.8.2-2
  source: xxhash
  source_version: 0.8.2-2
  arch: amd64
files:
  /usr/share/doc/libxxhash0/changelog.Debian.gz: |
    xxhash (0.8.2-2) unstable; urgency=medium

      * d/patches: fix libxxhash.pc.in. (Closes: #1051776). Thanks to
        Michael R. Crusoe <crusoe@debian.org> for the patch.

     -- Josue Ortega <josue@debian.org>  Tue, 12 Sep 2023 18:16:00 +0530

    xxhash (0.8.2-1) unstable; urgency=medium

      [ Debian Janitor ]
      * Trim trailing whitespace.
      * Update standards version to 4.6.1, no changes needed.

      [ Josue Ortega ]
      * New upstream release (0.8.2):
        + Refresh debian/patches.
      * debian/copyright:
        + Fix GPL-2+ version. Thanks to Bastian Germann
          <bage@debian.org>. (Closes: #1012005).
        + Update copyright years.
      * debian/watch: Update watch file. Thanks to
        Patrice Duroux <patrice.duroux@gmail.com> for the patch.
        Closes: #1050859.
      * debian/rules: Add hardening flags.
      * Bump Standards-Version to 4.6.2. No changes required.

     -- Josue Ortega <josue@debian.org>  Sun, 10 Sep 2023 15:19:16 +0530

    xxhash (0.8.1-1) unstable; urgency=medium

      * New maintainer. Closes: #1003524.
      * debian/copyright: Update copyright holder years for debian/*.
      * debian/control: Document Rules Requires Root as no.
      * Update package to use debhelper-compat = 13.
      * Bump Standards-Version to 4.6.0. No changes required.

     -- Josue Ortega <josue@debian.org>  Fri, 14 Jan 2022 08:49:06 -0600

    xxhash (0.8.1-0.1) unstable; urgency=medium

      * Non-maintainer upload.

      [ Norbert Preining ]
      * bump symbol versions to ensure that now 0.7 version interfers (LP: #1934992)

      [ Balint Reczey ]
      * New upstream version 0.8.1 (Closes: #1003073)
      * Drop upstream-b4dbf5fefc37b8a-fix-empty-version-in-pc-file patch.
        It is included in the new upstream release
      * Update symbols
      * Rework shipping man pages
      * Move packaging repository to Salsa
      * Orphaning the package according to Norbert's request.
        Thank you for the many years of work on the package!

     -- Balint Reczey <balint@balintreczey.hu>  Tue, 11 Jan 2022 11:19:45 +0100

    xxhash (0.8.0-2) unstable; urgency=medium

      * Use hardware-accelerated XXH3 computation on x86 architecture, and
        install the hardware-accelerating header file.
        Thanks to Julian Andres Klode. (Closes: #977345)
      * Cherry-pick upstream: Fix empty version in .pc file (Closes: #979758)

     -- Norbert Preining <norbert@preining.info>  Mon, 11 Jan 2021 21:57:57 +0900

    xxhash (0.8.0-1) unstable; urgency=medium

      * New upstream version 0.8.0
      * update patch
      * bump standards version, no changes necessary

     -- Norbert Preining <norbert@preining.info>  Mon, 10 Aug 2020 01:02:50 +0900

    xxhash (0.7.4-1) unstable; urgency=medium

      [ Debian Janitor ]
      * Set upstream metadata fields: Bug-Submit.

      [ Norbert Preining ]
      * New upstream version 0.7.4
      * Update symbols file

     -- Norbert Preining <norbert@preining.info>  Tue, 30 Jun 2020 17:10:59 +0900

    xxhash (0.7.3-2) unstable; urgency=medium

      [ Chris Lamb ]
      * Make the build reproducible (Closes: #956583)

     -- Norbert Preining <norbert@preining.info>  Mon, 13 Apr 2020 19:00:33 +0900

    xxhash (0.7.3-1) unstable; urgency=medium

      [ Debian Janitor ]
      * Trim trailing whitespace.
      * Bump debhelper from old 11 to 12.
      * Set debhelper-compat version in Build-Depends.
      * Set upstream metadata fields: Bug-Database, Repository, Repository-
        Browse.

      [ Norbert Preining ]
      * New upstream version 0.7.3
      * Install new files (pkg-config, header, new tool xxh128sum plus man)

     -- Norbert Preining <norbert@preining.info>  Fri, 13 Mar 2020 03:32:28 +0900

    xxhash (0.7.2-1) unstable; urgency=medium

      * New upstream version 0.7.2

     -- Norbert Preining <norbert@preining.info>  Sat, 12 Oct 2019 23:39:04 +0900

    xxhash (0.7.1-1) unstable; urgency=medium

      * New upstream version 0.7.1
      * Update symbols file

     -- Norbert Preining <norbert@preining.info>  Tue, 03 Sep 2019 10:36:52 +0900

    xxhash (0.7.0-2) unstable; urgency=medium

      * Upload to unstable

     -- Norbert Preining <norbert@preining.info>  Wed, 10 Jul 2019 17:37:07 +0900

    # Older entries have been removed from this changelog.
    # To read the complete changelog use `apt changelog libxxhash0`.
  /usr/share/doc/libxxhash0/changelog.gz: |
    v0.8.2
    - fix  : XXH3 S390x vector implementation (@hzhuang1)
    - fix  : PowerPC vector compilation with IBM XL compiler (@MaxiBoether)
    - perf : improved WASM speed by x2/x3 using SIMD128 (@easyaspi314)
    - perf : improved speed (+20%) for XXH3 on ARM NEON (@easyaspi314)
    - cli  : Fix filename contain /LF character (@t-mat)
    - cli  : Support # comment lines in --check files (@t-mat)
    - cli  : Support commands --binary and --ignore-missing (@t-mat)
    - build: fix -Og compilation (@easyaspi314, @t-mat)
    - build: fix pkgconfig generation with cmake (@ilya-fedin)
    - build: fix icc compilation
    - build: fix cmake install directories
    - build: new build options XXH_NO_XXH3, XXH_SIZE_OPT and XXH_NO_STREAM to reduce binary size (@easyaspi314)
    - build: dedicated install targets (@ffontaine)
    - build: support DISPATCH mode in cmake (@hzhuang1)
    - portability: fix x86dispatch when building with Visual + clang-cl (@t-mat)
    - portability: SVE vector implementation of XXH3 (@hzhuang1)
    - portability: compatibility with freestanding environments, using XXH_NO_STDLIB
    - portability: can build on Haiku (@Begasus)
    - portability: validated on m68k and risc-v
    - doc  : XXH3 specification (@Adrien1018)
    - doc  : improved doxygen documentation (@easyaspi314, @t-mat)
    - misc : dedicated sanity test binary (@t-mat)

    v0.8.1
    - perf : much improved performance for XXH3 streaming variants, notably on gcc and msvc
    - perf : improved XXH64 speed and latency on small inputs
    - perf : small XXH32 speed and latency improvement on small inputs of random size
    - perf : minor stack usage improvement for XXH32 and XXH64
    - api  : new experimental variants XXH3_*_withSecretandSeed()
    - api  : update XXH3_generateSecret(), can no generate secret of any size (>= XXH3_SECRET_SIZE_MIN)
    - cli  : xxhsum can now generate and check XXH3 checksums, using command `-H3`
    - build: can build xxhash without XXH3, with new build macro XXH_NO_XXH3
    - build: fix xxh_x86dispatch build with MSVC, by @apankrat
    - build: XXH_INLINE_ALL can always be used safely, even after XXH_NAMESPACE or a previous XXH_INLINE_ALL
    - build: improved PPC64LE vector support, by @mpe
    - install: fix pkgconfig, by @ellert
    - install: compatibility with Haiku, by @Begasus
    - doc  : code comments made compatible with doxygen, by @easyaspi314
    - misc : XXH_ACCEPT_NULL_INPUT_POINTER is no longer necessary, all functions can accept NULL input pointers, as long as size == 0
    - misc : complete refactor of CI tests on Github Actions, offering much larger coverage, by @t-mat
    - misc : xxhsum code base split into multiple specialized units, within directory cli/, by @easyaspi314

    v0.8.0
    - api : stabilize XXH3
    - cli : xxhsum can parse BSD-style --check lines, by @WayneD
    - cli : `xxhsum -` accepts console input, requested by @jaki
    - cli : xxhsum accepts -- separator, by @jaki
    - cli : fix : print correct default algo for symlinked helpers, by @martinetd
    - install: improved pkgconfig script, allowing custom install locations, requested by @ellert

    v0.7.4
    - perf: automatic vector detection and selection at runtime (`xxh_x86dispatch.h`), initiated by @easyaspi314
    - perf: added AVX512 support, by @gzm55
    - api : new: secret generator `XXH_generateSecret()`, suggested by @koraa
    - api : fix: XXH3_state_t is movable, identified by @koraa
    - api : fix: state is correctly aligned in AVX mode (unlike `malloc()`), by @easyaspi314
    - api : fix: streaming generated wrong values in some combination of random ingestion lengths, reported by @WayneD
    - cli : fix unicode print on Windows, by @easyaspi314
    - cli : can `-c` check file generated by sfv
    - build: `make DISPATCH=1` generates `xxhsum` and `libxxhash` with runtime vector detection (x86/x64 only)
    - install: cygwin installation support
    - doc : Cryptol specification of XXH32 and XXH64, by @weaversa

    v0.7.3
    - perf: improved speed for large inputs (~+20%)
    - perf: improved latency for small inputs (~10%)
    - perf: s390x Vectorial code, by @easyaspi314
    - cli: improved support for Unicode filenames on Windows, thanks to @easyaspi314 and @t-mat
    - api: `xxhash.h` can now be included in any order, with and without `XXH_STATIC_LINKING_ONLY` and `XXH_INLINE_ALL`
    - build: xxHash's implementation transferred into `xxhash.h`. No more need to have `xxhash.c` in the `/include` directory for `XXH_INLINE_ALL` to work
    - install: created pkg-config file, by @bket
    - install: VCpkg installation instructions, by @LilyWangL
    - doc: Highly improved code documentation, by @easyaspi314
    - misc: New test tool in `/tests/collisions`: brute force collision tester for 64-bit hashes

    v0.7.2
    - Fixed collision ratio of `XXH128` for some specific input lengths, reported by @svpv
    - Improved `VSX` and `NEON` variants, by @easyaspi314
    - Improved performance of scalar code path (`XXH_VECTOR=0`), by @easyaspi314
    - `xxhsum`: can generate 128-bit hashes with the `-H2` option (note: for experimental purposes only! `XXH128` is not yet frozen)
    - `xxhsum`: option `-q` removes status notifications

    v0.7.1
    - Secret first: the algorithm computation can be altered by providing a "secret", which is any blob of bytes, of size >= `XXH3_SECRET_SIZE_MIN`.
    - `seed` is still available, and acts as a secret generator
    - updated `ARM NEON` variant by @easyaspi314
    - Streaming implementation is available
    - Improve compatibility and performance with Visual Studio, with help from @aras-p
    - Better integration when using `XXH_INLINE_ALL`: do not pollute host namespace, use its own macros, such as `XXH_ASSERT()`, `XXH_ALIGN`, etc.
    - 128-bit variant provides helper functions for comparison of hashes.
    - Better `clang` generation of `rotl` instruction, thanks to @easyaspi314
    - `XXH_REROLL` build macro to reduce binary size, by @easyaspi314
    - Improved `cmake` script, by @Mezozoysky
    - Full benchmark program provided in `/tests/bench`
