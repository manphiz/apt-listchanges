import os

from utils import (
    install_yaml_package,
    make_config,
    package_from_yaml_file,
    setup_filterer,
)
from apt_listchanges.ALCLog import set_debug
from apt_listchanges.ALCSeenDb import SeenDb
from apt_listchanges.apt_listchanges import process_pkgs
from apt_listchanges.frontends import Frontend


# Test special handling for linux-image-* changelogs
def test_bug1055780(fs):
    testdir = os.path.dirname(__file__)
    fs.add_real_directory(testdir)
    old_yamls = ['linux-image-6.10.3-amd64_6.10.3-1_amd64.yaml',
                 'linux-image-6.10.4-amd64_6.10.4-1_amd64.yaml',
                 'linux-image-amd64_6.10.4-1_amd64.yaml']
    for pkg in old_yamls:
        install_yaml_package(fs, pkg)
    config = make_config({'no_network': False, 'which': 'both'})
    set_debug(True)
    frontend = Frontend(config, 1)
    seen_db = SeenDb()
    filterer = setup_filterer(seen_db=seen_db)
    new_yamls = ['linux-image-6.10.6-amd64_6.10.6-1_amd64.yaml',
                 'linux-image-amd64_6.10.6-1_amd64.yaml']
    pkgs = [package_from_yaml_file(f, filterer=filterer) for f in new_yamls]
    (news, _changelog) = process_pkgs(config, frontend, seen_db, pkgs)
    assert _changelog.endswith('Mon, 19 Aug 2024 21:59:45 +0200\n')
