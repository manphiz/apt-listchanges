import os

from utils import (
    install_yaml_package,
    make_config,
    package_from_yaml_file,
    setup_filterer,
)
from apt_listchanges.ALCLog import set_debug
from apt_listchanges.ALCSeenDb import SeenDb
from apt_listchanges.apt_listchanges import process_pkgs
from apt_listchanges.frontends import Frontend


# Make sure we display changelog entries for binary NMUs
def test_bug1063677(fs):
    testdir = os.path.dirname(__file__)
    fs.add_real_directory(testdir)
    old_yamls = ['libxxhash0_0.8.2-2_amd64.yaml']
    # pylint: disable=duplicate-code
    for pkg in old_yamls:
        install_yaml_package(fs, pkg)
    config = make_config({'no_network': False, 'which': 'both'})
    set_debug(True)
    frontend = Frontend(config, 1)
    seen_db = SeenDb()
    filterer = setup_filterer(seen_db=seen_db)
    new_yamls = ['libxxhash0_0.8.2-2+b1_amd64.yaml']
    pkgs = [package_from_yaml_file(f, filterer=filterer) for f in new_yamls]
    (news, _changelog) = process_pkgs(config, frontend, seen_db, pkgs)
    assert '0.8.2-2+b1' in _changelog
